
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var id1 = "8f7dc280-28fa-48a2-ae9b-f2c91d59e52d";
var id2 = "c27b5087-7bd0-460e-94f0-d276f57c8c3e";
var id3 = "93c2a005-1a25-476e-8786-f83ac57afa43";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacientaloca
 */
function generirajPodatke(stPacienta) {
	//******
	sessionId = getSessionId();
	var ime;
	var priimek;
	var datumRojstva;

	switch(stPacienta){
		case 1:
			ime = "Janez";
			priimek ="Novak";
			datumRojstva ="1956-06-06"+ "T00:00:00.000Z";
			break;
		case 2:
			ime = "Domen";
			priimek ="Krajcar";
			datumRojstva ="2000-03-05"+ "T00:00:00.000Z";
			break;
		case 3:
			ime = "Zala";
			priimek ="Potepuh";
			datumRojstva ="1983-09-10"+ "T00:00:00.000Z";
			break;
	}

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
				var ehrId = data.ehrId;
				IDmoj = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
				$.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
							switch(stPacienta){
								case 1:
		                    		$("#default1").html("<span class='obvestilo " +
                          				"label label-success fade-in'>Uspešno kreiran EHR '" +
                          			ehrId + "'.</span>");
									$("#preberiEHRid").val(ehrId);
									id1=ehrId;
							break;
							case 2:
		                    		$("#default2").html("<span class='obvestilo " +
                          				"label label-success fade-in'>Uspešno kreiran EHR '" +
                          			ehrId + "'.</span>");
									$("#preberiEHRid").val(ehrId);
									id2=ehrId;
							break;
							case 3:
		                    		$("#default3").html("<span class='obvestilo " +
                          				"label label-success fade-in'>Uspešno kreiran EHR '" +
                          			ehrId + "'.</span>");
									$("#preberiEHRid").val(ehrId);
									id3=ehrId;
							break;
						}
							
							//********************** */
							dodajPodatke(ehrId, stPacienta);
							return ehrId;
							//********************** */
		                }
		            },
		            error: function(err) {
		            	$("#bla").html("<span class='obvestilo'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
			}
		});
		
}

function pridobiVreme(){

	$.ajax({
		type: "GET",               
		   url: "https://api.openweathermap.org/data/2.5/weather?q="+ document.getElementById("kraj").value +"&units=metric&APPID=2176a49a2a75941fba27fb97648c24e9",
	   error: function (response) {
				alert('Ne najdem kraja. Preverite zapis.');
		},

			success: function (response) {
			$("#casZaTelovadbo").empty();
			var minmax = [response.main.temp_max, response.main.temp_min];
			var vremeDanes = response.weather[0].description;
			var dez = response.weather[0].id;

			if(minmax[0] < 15){
				$("#casZaTelovadbo").html("<span class='obvestilo'>DANES NI ČAS ZA TELOVADBO (premraz je: "+ minmax[0] +"C)</span>");
			}else{
				if(minmax[0] > 26){
					$("#casZaTelovadbo").html("<span class='obvestilo'>DANES NI ČAS ZA TELOVADBO (prevroče je: "+ minmax[0] +"C)</span>");
				}else{
					if(dez >= 200 && dez <= 700){
						$("#casZaTelovadbo").html("<span class='obvestilo'>DANES NI ČAS ZA TELOVADBO (vreme: "+ vremeDanes +")</span>");
					}else{
						$("#casZaTelovadbo").html("<span class='obvestilo'>DANES JE ČAS ZA TELOVADBO (Je prijetnih "+ minmax[0] +"C)</span>");
					}
				}
			}

		   }
	});

	   }

function dodajPodatke(ehrId, st){
	var telesnaVisina;
	var telesnaTeza;
	var telesnaTemperatura;
	var sistolicniKrvniTlak;
	var diastolicniKrvniTlak;
	var nasicenostKrviSKisikom;

	switch(st){
		case 1:
			var telesnaVisina = 183;
			var telesnaTeza = 63;
			var telesnaTemperatura = 36;
			var sistolicniKrvniTlak = 110;
			var diastolicniKrvniTlak = 80;
			var nasicenostKrviSKisikom = 99;
		break;
		case 2:
			var telesnaVisina = 200;
			var telesnaTeza = 115;
			var telesnaTemperatura = 36;
			var sistolicniKrvniTlak = 110;
			var diastolicniKrvniTlak = 80;
			var nasicenostKrviSKisikom = 99;
		break;
		case 3:
			var telesnaVisina = 163;
			var telesnaTeza = 95;
			var telesnaTemperatura = 36;
			var sistolicniKrvniTlak = 110;
			var diastolicniKrvniTlak = 80;
			var nasicenostKrviSKisikom = 99;
		break;
	}
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
			format: 'FLAT'
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		    },
		    error: function(err) {
		    	$("#bla").html(
            "<span class='obvestilo'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
}

function prikaziPacienta(vnaprej){
	
	sessionId = getSessionId();

	var ehrId;
	if(vnaprej != 0){
		switch(vnaprej){
			case 1:
			ehrId = id1;
			break;
			case 2:
			ehrId = id2;
			break;
			case 3:
			ehrId = id3;
			break;
		}
	}else{
		ehrId = document.getElementById("bolnik-vpisID").value;
	}

	if (ehrId.length == 0) {
	  console.log("error1");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
			success: function(data){
				var ime = data.party.firstNames;
				var priimek = data.party.lastNames;
				var datum = data.party.dateOfBirth;
				var x = document.getElementById("imeD");
				x.innerHTML = ime;
				var y = document.getElementById("priimekD");
				y.innerHTML = priimek;
				var z = document.getElementById("dRojstvaD");
				z.innerHTML = datum;
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + "weight",
					type: 'GET',
					headers: {"Ehr-Session": sessionId},
						success: function (res) {
							var teza = res[0].weight;
							$.ajax({
								url: baseUrl + "/view/" + ehrId + "/" + "height",
								type: 'GET',
								headers: {"Ehr-Session": sessionId},
									success: function (res) {
										d3.selectAll("#chart1 > *").remove();
										d3.selectAll("#chart2 > *").remove();
										$("#obvestilo").empty();
										var visina = res[0].height;
										var dataTeza = [["Teža pacienta: ",teza], ["Najmanjša zdrava teža: ",Math.round(visina*visina*18.5/10000)],["Največja še zdrava teža: ", Math.round(visina*visina*24.99/10000)]];
										var dataVisina = [["Višina pacienta: ",visina],["Povprečna višina v Sloveniji: ", 173.5]];

										d3.select("#chart1")
										.selectAll("div")
										.data(dataVisina)
											.enter()
											.append("div")
											.style("width", function(d) { return d[1]/5 + "%"; })
											.text(function(d) { return d[0] +''+d[1]; });

										d3.select("#chart2")
										.selectAll("div")
										.data(dataTeza)
											.enter()
											.append("div")
											.style("width", function(d) { return d[1]/3 + "%"; })
											.text(function(d) { return d[0]+''+d[1]; });
										
										if(dataTeza[0][1] > dataTeza[2][1])
										$("#obvestilo").html("<span class='obvestilo'>Vaša teža je prevelika. Vpišite kraj in poglejte če je danes priložnost za telovadbo.</span>");
									},
									error: function(err) {
										console.log("error2");
									}
							});
						},
						error: function(err) {
							console.log("error1")
						}
				});
				
				
			},
			
			error: function(err){
				console.log("error3");
			}
		});
	}
}

function skrij(kajSkrijem){
	if(kajSkrijem == 1)
	if(document.getElementById("podatkiSkrij").style.display != "block"){
		document.getElementById("podatkiSkrij").style.display = "block";
	}else{
		document.getElementById("podatkiSkrij").style.display = "none";
	}
	if(kajSkrijem == 2)
	if(document.getElementById("chart1").style.display != "block"){
		document.getElementById("chart1").style.display = "block";
	}else{
		document.getElementById("chart1").style.display = "none";
	}
	if(kajSkrijem == 3)
	if(document.getElementById("chart2").style.display != "block"){
		document.getElementById("chart2").style.display = "block";
	}else{
		document.getElementById("chart2").style.display = "none";
	}
}